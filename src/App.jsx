import React, { useEffect } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import firebase from './config/firebase';

import Dashboard from './components/Dashboard/Dashboard';
import Register from './components/Auth/Register';
import Login from './components/Auth/Login';

import { setUser } from './redux/actionCreators/userActions';

const mapActionsToProps = {
  setUser,
} 

const App = ({setUser}) => {

  useEffect( () => {
    /** If Firebase auth was successful and a user logged in, we will have a token
     * that we're going to read out here and if it exists, we will run the setUser
     * action creator that passes the data to the user reducer. If no token exists,
     * then we send the same function with null as value. We do that in order to
     * set the isLoading flag to false because we initially set it to true.
     */

    firebase.auth().onAuthStateChanged( loggedInUser => {
      if(loggedInUser){
        setUser(loggedInUser);
      } else {
        setUser(null);
      }
    })//if

  }, [])
 
  return (
    <BrowserRouter>
      <Switch>
        <Route path='/' component={Dashboard} exact />
        <Route path='/register' component={Register} />
        <Route path='/login' component={Login} />
      </Switch>
    </BrowserRouter>
  )//return
}

export default connect(null, mapActionsToProps)(App);
