import React, {useState, useEffect} from 'react';
import firebase from '../../config/firebase';
import md5 from 'md5'; //for hashing of the photoURL
import { connect } from 'react-redux';

import { Grid, Form, Segment, Button, Header, Message, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

import Spinner from '../shared/Spinner/Spinner';

import { 
  isRegisterFormEmpty, 
  isUsernameValid, 
  isEmailValid, 
  isPasswordValid 
} from './validatorFunctions';

//==========================================

const mapStateToProps = state => ({
  isUserInfoLoading: state.user.isLoading,  
  isUserLoggedIn: !!state.user.currentUser,
});

//==========================================

const Register = ({isUserInfoLoading, isUserLoggedIn, history}) => {

  useEffect( () => {
    /* When this component mounts, we check if there is already a token from firebase
    indicating that a user is currently logged in. If so, then there will be user info
    available in the currentUser object of the user reducer. In that case we use the 
    history object that we get via props from the BrowserRouter in App.jsx to push 
    the user to the home page. We want to make the decision each time that isUserLoggedIn
    changes so that's why we provide the variable in the second argument of this fct. */

    if(isUserLoggedIn){
      history.push('/');
    }

  }, [isUserLoggedIn]);

  //defining state properties and setters:
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');  
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');
  const [registerErrors, setRegisterErrors] = useState([]);
  const [loading, setLoading] = useState(false); //for the loading indicator of the submit button

  //defining handlers:
  const onUsernameChange = e =>             setUsername(e.target.value);
  const onEmailChange = e =>                setEmail(e.target.value);  
  const onPasswordChange = e =>             setPassword(e.target.value);
  const onPasswordConfirmationChange = e => setPasswordConfirmation(e.target.value);

  //==========================================

  const showRegisterErrors = () => 
    registerErrors.map( (error, idx) => <p key={idx}>{error.message}</p> );

  //==========================================

  /* Styling for fields in case of error: if the inputName (username, email, password etc) 
  is included in the error message (for example: password invalid), then return the string
  'error' (or nothing in case that the name is not included). This will provide error
  styling via symantic-ui-css */

  const showInputErrorStyle = inputName =>
    registerErrors.some( error => error.message.toLowerCase().includes(inputName) ) ? 'error' : null;

  //==========================================

  const saveUserProfile = createdUserObj => {
    /* Access the reference to the userProfiles collection and get the child with
    the name of the uid of the new created user and if it does not exist, which
    will be true here, then create it. Then set displayName and photoURL for that
    profile. All this returns a promise.
    */
    return firebase.database().ref('userProfiles').child(createdUserObj.user.uid).set({
      displayName: createdUserObj.user.displayName,
      photoURL: createdUserObj.user.photoURL
    })
  }//saveUserProfile

  //==========================================

  const onFormSubmit = e => {
    e.preventDefault(); //prevent a page reload

    //set registerErrors to empty array in case that the user clicks on submit again
    //See: https://stackoverflow.com/questions/54163796/react-usestate-hook-setter-has-no-result-with-array-variable    
    setRegisterErrors([]);

    //set loading flag to true to indicate that auth is in loading status
    setLoading(true);

    let error;

    //error checking:
    if( isRegisterFormEmpty(username, email, password, passwordConfirmation) ) {

      error = {message: 'Please fill in all fields'};
      setRegisterErrors(registerErrors => [...registerErrors, error]);
      setLoading(false);

    } else if(!isEmailValid(email)){

      error = {message: 'Invalid email'};
      setRegisterErrors(registerErrors => [...registerErrors, error]); 
      setLoading(false); 

    } else if (!isUsernameValid(username)) {

      error = {message: 'Username must have at least 3 characters'};
      setRegisterErrors(registerErrors => [...registerErrors, error]); 
      setLoading(false); 

    } else if (!isPasswordValid(password, passwordConfirmation)) {

      error = {message: 'Password and password confirmation must have at least 6 characters and match each other'};
      setRegisterErrors(registerErrors => [...registerErrors, error]); 
      setLoading(false);

    } else {

        //create the user based on email and password:
        firebase
          .auth()
          .createUserWithEmailAndPassword(email, password)
          .then( createdUserObj => {
            
            //=====================

            //update displayName and photoURL of the user profile
            //The photoURL uses gravatar to generate a random profile picture or to use the user's
            //profile picture in case that he's provided it on gravatar.com
          createdUserObj.user
            .updateProfile({
              displayName: username,
              photoURL: `https://gravatar.com/avatar/${md5(createdUserObj.user.email)}?d=identicon`
            })
            .then( () => {  
              
              //save user profile on realtime DB
              saveUserProfile(createdUserObj)
                .then( () => {
                  console.log("User saved");
                  setLoading(false);
                  })
              
            })
            .catch(error => {
              setRegisterErrors(registerErrors => [...registerErrors, error]);
              setLoading(false);
            });

          })//createUserWithEmailAndPassword then
            //=====================

          //catching auth errors from firebase:
          .catch( error => {
            setRegisterErrors(registerErrors => [...registerErrors, error]);
            setLoading(false);
          });

    }//else
  }//onFormSubmit


  //==========================================

  if (isUserInfoLoading) return <Spinner/>;
  
  return (
    <Grid textAlign='center' verticalAlign='middle' className='app'>
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as='h2' icon color='teal' textAlign='center'>
          <Icon name='puzzle piece' color='teal' />
          Register to DevChat
        </Header>
        <Form size='large' onSubmit={onFormSubmit}>
          <Segment stacked>
            <Form.Input 
              fluid
              type='text'              
              icon='user'
              iconPosition='left'
              placeholder='Username'
              onChange={onUsernameChange}
              value={username}
              className={showInputErrorStyle('username')}
            />
            <Form.Input 
              fluid
              type='email'              
              icon='mail'
              iconPosition='left'
              placeholder='Email'
              onChange={onEmailChange}
              value={email}
              className={showInputErrorStyle('email')}
            />
            <Form.Input 
              fluid
              type='password'              
              icon='lock'
              iconPosition='left'
              placeholder='Password'
              onChange={onPasswordChange}
              value={password}
              className={showInputErrorStyle('password')}
            />
            <Form.Input 
              fluid
              type='password'              
              icon='lock'
              iconPosition='left'
              placeholder='Password Confirmation'
              onChange={onPasswordConfirmationChange}
              value={passwordConfirmation}
              className={showInputErrorStyle('password')}
            />  
            <Button
              color='teal'
              fluid
              size='large'
              loading={loading}
              disabled={loading}
              content='Submit'
            />          
          </Segment>
        </Form>
        {
          registerErrors.length > 0 && 
            <Message error>
              <h3>Please note</h3>
              {showRegisterErrors()}
            </Message>
        }
        <Message>
            Already a user? <Link to='/login'>Login</Link>
        </Message>
      </Grid.Column>
    </Grid>
  )
}

export default connect(mapStateToProps)(Register);