import React, {useState, useEffect} from 'react';
import firebase from '../../config/firebase';
import { connect } from 'react-redux';

import { Grid, Form, Segment, Button, Header, Message, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

import Spinner from '../shared/Spinner/Spinner';

//==========================================

const mapStateToProps = state => ({
  isUserInfoLoading: state.user.isLoading,  
  isUserLoggedIn: !!state.user.currentUser,
});

//==========================================

const Login = ({isUserInfoLoading, isUserLoggedIn, history}) => {

  console.log(isUserLoggedIn)

  useEffect( () => {
    /* When this component mounts, we check if there is already a token from firebase
    indicating that a user is currently logged in. If so, then there will be user info
    available in the currentUser object of the user reducer. In that case we use the 
    history object that we get via props from the BrowserRouter in App.jsx to push 
    the user to the home page. We want to make the decision each time that isUserLoggedIn
    changes so that's why we provide the variable in the second argument of this fct. */

    if(isUserLoggedIn){
      history.push('/');
    }

  }, [isUserLoggedIn]);


  //defining state properties and setters:
  const [email, setEmail] = useState('');  
  const [password, setPassword] = useState('');
  const [loginErrors, setLoginErrors] = useState([]);
  const [loading, setLoading] = useState(false); //for the loading indicator of the submit button

  //defining handlers:
  const onEmailChange = e =>                setEmail(e.target.value);  
  const onPasswordChange = e =>             setPassword(e.target.value);

  //==========================================

  const showLoginErrors = () => 
    loginErrors.map( (error, idx) => <p key={idx}>{error.message}</p> );

  //==========================================

  /* Styling for fields in case of error: if the inputName (email, password etc) 
  is included in the error message (for example: email invalid), then return the string
  'error' (or nothing in case that the name is not included). This will provide error
  styling via symantic-ui-css */

  const showInputErrorStyle = inputName =>
    loginErrors.some( error => error.message.toLowerCase().includes(inputName) ) ? 'error' : null;

  //==========================================

  const onFormSubmit = e => {
    e.preventDefault(); //prevent a page reload

    //set loginErrors to empty array in case that the user clicks on submit again
    setLoginErrors([]);    

    if( email.length && password.length ) { 

        //set loading flat to true to indicate that auth is in loading status
        setLoading(true);

        firebase
          .auth()
          .signInWithEmailAndPassword(email, password)
          .then( signedInUser => {
            console.log("signedInUser", signedInUser);
            setLoading(false);
          })
          .catch( error => {
            //catching auth errors from firebase:
            setLoginErrors(loginErrors => [...loginErrors, error]);
            setLoading(false);
          });

    } else {
      let error = {message: 'Please fill in both fields'};
      setLoginErrors( loginErrors => [...loginErrors, error] );
      setLoading(false);   
    }

  }//onFormSubmit


  //==========================================

  if (isUserInfoLoading) return <Spinner/>;

  return (
    <Grid textAlign='center' verticalAlign='middle' className='app'>
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as='h2' icon color='purple' textAlign='center'>
          <Icon name='puzzle piece' color='purple' />
          Login to DevChat
        </Header>
        <Form size='large' onSubmit={onFormSubmit}>
          <Segment stacked>
            <Form.Input 
              fluid
              type='email'              
              icon='mail'
              iconPosition='left'
              placeholder='Email'
              onChange={onEmailChange}
              value={email}
              className={showInputErrorStyle('identifier')}
            />
            <Form.Input 
              fluid
              type='password'              
              icon='lock'
              iconPosition='left'
              placeholder='Password'
              onChange={onPasswordChange}
              value={password}
              className={showInputErrorStyle('password')}
            />
            <Button
              color='purple'
              fluid
              size='large'
              loading={loading}
              disabled={loading}
              content='Submit'
            />          
          </Segment>
        </Form>
        {
          loginErrors.length > 0 && 
            <Message error>
              <h3>Please note</h3>
              {showLoginErrors()}
            </Message>
        }
        <Message>
            Don't have an account? <Link to='/register'>Register</Link>
        </Message>
      </Grid.Column>
    </Grid>
  )
}

export default connect(mapStateToProps)(Login);