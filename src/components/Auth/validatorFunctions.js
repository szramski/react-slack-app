export const isRegisterFormEmpty = (
  username, 
  email, 
  password, 
  passwordConfirmation
) => {
  if( !username.length || !email.length || !password.length || !passwordConfirmation.length ) {
    //if there is no length of any of these fields, they're empty
    return true
  } else {
    return false
  }
}


export const isEmailValid = email => {
  var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  return re.test(String(email).toLowerCase());
}


export const isUsernameValid = username =>  username.length >= 3 ? true : false


export const isPasswordValid = (
  password, 
  passwordConfirmation
) => {
  if (password.length < 6 || passwordConfirmation.length < 6){
    return false
  } else if (password !== passwordConfirmation) {
    return false
  } else {
    return true
  }
} 
