import React, {useEffect} from 'react';
import { connect } from 'react-redux';
import { Grid } from 'semantic-ui-react';

import Spinner from '../shared/Spinner/Spinner';

import ColorPanel from './ColorPanel/ColorPanel';
import SidePanel from './SidePanel/SidePanel';
import Messages from './Messages/Messages';
import MetaPanel from './MetaPanel/MetaPanel';

//==========================================

const mapStateToProps = state => ({
  isUserInfoLoading: state.user.isLoading,  
  isUserLoggedIn: !!state.user.currentUser,
});

//==========================================

const Dashboard = ({isUserInfoLoading, isUserLoggedIn, history}) => {

  useEffect( () => {
    /* When this component mounts, we check if there is already a token from firebase
    indicating that a user is currently logged in. If so, then there will be user info
    available in the currentUser object of the user reducer. 
    But if the user info doesn't exist, then we use the history object that we get 
    via props from the BrowserRouter in App.jsx to push the user to the login page */

    if(!isUserLoggedIn){
      history.push('/login');
    }

  }, []);

  if (isUserInfoLoading) return <Spinner/>

  return (
    <Grid columns='equal' className='app'>
      <ColorPanel />
      <SidePanel />
      <Grid.Column style={{marginLeft: 320}}>
        <Messages />
      </Grid.Column>
      <Grid.Column width={4}>
        <MetaPanel />  
      </Grid.Column>
    </Grid>
  )

}//Dashboard

export default connect(mapStateToProps)(Dashboard);