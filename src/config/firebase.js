import firebase from 'firebase/app';

//importing specific firebase modules
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';

import keys from '../config/keys';

// configure Firebase
firebase.initializeApp(keys.firebaseConfig);

export default firebase;