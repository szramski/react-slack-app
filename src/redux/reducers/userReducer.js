import * as actionTypes from '../actionCreators/actionTypes';

const initialState = {
    currentUser: null,
    isLoading: true
}

const userReducer = (state = initialState, action) => {
  switch(action.type) {
    case actionTypes.SET_USER:
      return {
        currentUser: action.payload.currentUser,
        isLoading: false
      }
    default:
      return state
  }//switch
}//userReducer

export default userReducer;