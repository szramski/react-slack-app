import * as actionTypes from './actionTypes';

export const setUser = currentUser => {

  return {
    type: actionTypes.SET_USER,
    payload: { 
      currentUser
    }   
  }
} //setUser