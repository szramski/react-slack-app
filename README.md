Szramski's interpretation of the code seen on:

https://www.udemy.com/build-a-slack-chat-app-with-react-redux-and-firebase/

(React Hooks rock!)

Important hint: In the onFormSubmit method of the Register component,
I had some trouble reseting a state variable (with useState hook) back to 
an empty array. A post on Stackoverflow solved the issue:

https://stackoverflow.com/questions/54163796/react-usestate-hook-setter-has-no-result-with-array-variable